// Toggle mobile nav
$(".burger-icon").on("click", function() {
	$(this).toggleClass("open");
	$("body").toggleClass("nav-is-open");
	return false;
});



// Animate text when in viewport
var $appear = $(".content-block--animate");

$appear.each(function (ignore) {
	var waypoint = new Waypoint( {
		element: $(this),
		handler: function() { 
			// console.log('Scrolled to waypoint!')
			$(this.element).addClass('animate')
		},
		offset: '50%',
		once: true
	} );
});

// Animate images when in viewport
var $zoomers = $(".img-zoom-wrapper");

$zoomers.each(function (ignore) {
	var waypoint = new Waypoint( {
		element: $(this),
		handler: function() { 
			// console.log('Scrolled to waypoint!')
			$(this.element).addClass('zoom-in')
		},
		offset: '50%',
		once: true
	} );
});