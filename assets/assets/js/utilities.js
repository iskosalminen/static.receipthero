// 60fps scrolling using pointer-events: none
// http://www.thecssninja.com/javascript/pointer-events-60fps
// You need to add this to your CSS:
// .disable-hover, .disable-hover * { pointer-events: none !important; }
var body = document.body,
	timer;

window.addEventListener('scroll', function () {
	clearTimeout(timer);
	if (!body.classList.contains('disable-hover')) {
		body.classList.add('disable-hover');
	}

	timer = setTimeout(function () {
		body.classList.remove('disable-hover');
	}, 250);
}, false);



// Smooth scrolling on ID links
$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function (event) {
		// On-page links
		if (
			location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
			location.hostname == this.hostname
		) {
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000, function () {
					// Callback after animation
					// Must change focus!
					var $target = $(target);
					$target.focus();
					if ($target.is(":focus")) { // Checking if the target was focused
						return false;
					} else {
						$target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
						$target.focus(); // Set focus again
					};
				});
			}
		}
	});



// Auto-resize textarea as user writes
// $('textarea').each(function () {
// 	this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
// }).on('input', function () {
// 	this.style.height = 'auto';
// 	this.style.height = (this.scrollHeight) + 'px';
// });